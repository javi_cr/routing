import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  @Input() object: any;
  @Input() service: any;

  is_confirming = false;
  constructor() { }

  ngOnInit() {
  }

  confirm() {
    this.service.deleteById(this.object.id);
  }
}
