import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WachiGuard implements CanActivate {
  constructor(private router: Router) {
    // code...
  }
  canActivate() {
    this.router.navigateByUrl('/error');
    return false;
  }
}
