export class Article {
  id: number;
  name: string;
  price: number;
  image_url: string;
  constructor(id, name, price, image_url) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.image_url = image_url;
  }
}
